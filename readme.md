see https://github.com/jollheef/nixos-conf

```sh
parted /dev/sda
(parted) mklabel msdos
(parted) mkpart primary ext4 0% 100%
(parted) quit

export NIXOS_NAME=""
cryptsetup luksFormat /dev/sda1
cryptsetup luksOpen /dev/sda1 ${NIXOS_NAME}
mkfs.ext4 -L ${NIXOS_NAME} /dev/mapper/${NIXOS_NAME}
mount /dev/mapper/${NIXOS_NAME} /mnt

nix-channel --update
nix-env -i git
mkdir /tmp/nixos
git clone https://smalldave@bitbucket.org/smalldave/nixos /tmp/nixos
nixos-generate-config --root /mnt
cp -Tr /tmp/nixos/etc /mnt/etc/nixos
sed -i "s/thiq/${NIXOS_NAME}/g" /mnt/etc/nixos/configuration.nix
nixos-install
reboot

cd /etc/nixos && git reset --hard
```

OLD
```sh
curl -su smalldave https://bitbucket.org/smalldave/nixos/get/master.tar.gz | tar -xvz -C /tmp/nixos

#TODO: boot partition
parted -a optimal /dev/sda1 mkpart primary 0% 100%
mkfs.ext4 -j -L nixos /dev/sda1
mount LABEL=nixos /mnt
nixos-generate-config --root /mnt

cp /tmp/nixos/configuration.nix /mnt/etc/nixos/configuration.nix
#TODO: How do we change user password before reboot?
#TODO: Copy dotfiles
```

#Sound problems

See [](https://discourse.nixos.org/t/cant-get-alsa-nixos-working/644/2)

Need to create the /etc/asound.conf and check for muted channels in alsamixer

#Yubikey and GPG

```sh
curl https://keybase.io/smalldave/pgp_keys.asc | gpg --import
gpg2 --card-status
```
