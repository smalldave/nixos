{ config, pkgs, ... }:

{
  imports =
    [ 
      ./common.nix
    ];

  networking = {
    wireless = {
      enable = true;
    };
  };

  hardware.bluetooth.enable = true;

  services = {
    xserver.libinput = {
      enable = true;
      touchpad = {
        accelSpeed = ".25";
        middleEmulation = true;
        naturalScrolling = true;
        scrollMethod = "twofinger";
        tapping = true;
      };
    };
    logind.extraConfig =
      "HandlePowerKey=ignore";
  };
}
