{ config, pkgs, ... }:

{
  imports =
    [ 
      ./common.nix
      ./home.nix
    ];

  boot.loader.grub.device = "/dev/sda";

  networking = {
    hostName = "dave-pc"; 
  };

  services.xserver = {
    videoDrivers = [ "nvidia" ];
  };

  environment.systemPackages = with pkgs; [
    compton
  ];

  services.compton = {
    enable = true;
    fade = false;
    shadow = false;
    backend = "glx";
    vSync = true; 
  };  

  hardware.pulseaudio = {
    enable = true;
    support32Bit = true;
  };
}
