{ config, pkgs, ... }:

{
  imports =
    [ 
      ./laptop.nix
    ];

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      device = "nodev";
    };
    systemd-boot.enable = true;
  };

  boot.initrd = {
    luks.devices.root = {
      device="/dev/disk/by-uuid/{{luks_uuid}}";
      preLVM = true;
      allowDiscards = true;
    };
  };
  #TODO: Swap, trim, disk options? See https://github.com/jollheef/nixos-conf/blob/master/configuration.nix

  networking = {
    hostName = "dave-chromebook"; 
  };

  console.keyMap = "uk";

  services.xserver = {
    layout = "gb";
    videoDrivers = [ "intel" ];
    deviceSection = ''
      Option "DRI" "2"
      Option "TearFree" "true"
    '';
  };

  #zramSwap = {
  #  enable = true;
  #  algorithm = "zstd";
  #};

  environment.systemPackages = with pkgs; [
    compton
  ];

}
