{ config, pkgs, ... }:
{
  imports = [ <home-manager/nixos> ];

  home-manager.users.dave = { pkgs, ... }: {
    programs = {
      #urxvt;

      #vscode = {
      #  enable = true;
      #  package = pkgs.vscodium;    # You can skip this if you want to use the unfree version
      #  extensions = with pkgs.vscode-extensions; [
      #    # Some example extensions...
      #    dracula-theme.theme-dracula
      #    vscodevim.vim
      #    yzhang.markdown-all-in-one
      #  ];
      #};
    };
  };
}
