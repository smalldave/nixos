{ config, pkgs, ... }:

{
  imports =
    [ 
      ./laptop.nix
      ./rc.nix
    ];

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      device = "nodev";
      efiSupport = true;
      #extraInitrd = /boot/initrd.keys.gz;
      version = 2;
    };
  };

  boot.initrd = {
    luks.devices.root = {
      device="/dev/disk/by-uuid/1959bc65-39f5-4711-8b3d-999de65af7d5";
      preLVM = true;
      keyFile = "/keyfile0.bin";
      allowDiscards = true;
    };
    secrets = {
      "/keyfile0.bin" = "/etc/secrets/initrd/keyfile0.bin";
    };
  };
  #TODO: Swap, trim, disk options? See https://github.com/jollheef/nixos-conf/blob/master/configuration.nix

  networking = {
    hostName = "dave-chromebook"; 
  };

  console.keyMap = "uk";

  services.xserver = {
    layout = "gb";
    videoDrivers = [ "intel" ];
    deviceSection = ''
      Option "DRI" "2"
      Option "TearFree" "true"
    '';
  };

  #zramSwap = {
  #  enable = true;
  #  algorithm = "zstd";
  #};

  environment.systemPackages = with pkgs; [
    compton
  ];

}
