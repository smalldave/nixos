{ config, pkgs, ... }:

{
  imports =
    [ 
      ./common.nix
    ];

  networking = {
    hostName = "dave-vm"; 
  };
}
