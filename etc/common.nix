{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> { config.allowUnfree = true; };
in
{
  imports =
    [
      ./hardware-configuration.nix
    ];


  nixpkgs.config.allowBroken = true;

  time.timeZone = "Europe/London";
  i18n = {
    #defaultLocale = "en_GB.utf8";
  };

  hardware.ledger.enable = true;

  services = {
    avahi = {
      enable = true;
      nssmdns = true;
    };
    pcscd.enable = true;
    printing.enable = true;
    xserver = {
      desktopManager = {
        xterm.enable = false;
      };
      displayManager = {
        defaultSession = "none+i3";
      };
      enable = true;
      windowManager.i3.enable = true;
      xkbOptions = "caps:hyper";
    };
    udev.packages = with pkgs; [
      yubikey-personalization
    ];  
  };

  users.users.dave = {
    isNormalUser = true;
    home = "/home/dave";
    description = "David Smith";
    extraGroups = [ "adbusers" "audio" "dialout" "docker" "wheel" "vboxusers" ];
  };

  nixpkgs.config = {
    allowUnfree = true;
  };

  fonts = {
    fontDir.enable = true;
    fontconfig = {
      cache32Bit = true;
      allowBitmaps = true;
      useEmbeddedBitmaps = true;
      defaultFonts = {
        monospace = [ "Inconsolata" ];
      };
    };
    fonts = with pkgs; [
      font-awesome
    ];
  };

  hardware.pulseaudio.enable = true; 
  sound.enable = true;

  programs = {
    adb.enable = true;
    ssh.startAgent = false;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  environment.systemPackages = with pkgs; [
      autocutsel
      awscli
      bc
      bind
      unstable.brave
      unstable.chromium
      cifs-utils
      ctags
      (cura.override { plugins = [ curaPlugins.octoprint ]; })
      unstable.discord
      dmenu
      dunst
      exfat
      firefox
      #firejail
      git
      gimp
      gnupg 
      unstable.googler
      htop
      i3
      i3lock
      i3status
      jq
      unstable.ledger
      unstable.ledger-live-desktop
      libfaketime
      libnotify
      lm_sensors
      gnumake42
      unstable.mypy
      mupdf
      ncdu
      openconnect
      openscad
      orion
      pass
      pciutils
      powertop
      pavucontrol
      rclone
      pypi2nix
      remmina
      #skype
      spotify
      rofi
      teams
      tdesktop
#      (unstable.terraform_0_15.withPlugins (p : [p.aws p.digitalocean]))
      tree
      rxvt_unicode
      unclutter
      unzip
      (vim_configurable.customize {
        name = "vim";
        vimrcConfig.vam.knownPlugins = pkgs.vimPlugins;
        vimrcConfig.vam.pluginDictionaries = [
          { names = [
            "ctrlp"
            "editorconfig-vim"
            "vim-polyglot"
            "vim-nix"
          ]; }
        ];
      })
      unstable.vpn-slice
      xclip
      xorg.xbacklight
      xorg.xhost
      yubikey-personalization
      zip
  ];

  system.stateVersion = "18.03"; # Did you read the comment?

  virtualisation = {
    docker.enable = true;
    virtualbox.host.enable = true;
  };

  networking.wireless.userControlled.enable = true;

}
