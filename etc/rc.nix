{config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  programs.chromium = {
    extensions = [
      "oamephloldipgipaekfpmmnjocadecgc" # blheli configurator
    ];
  };

  # plugdev required to allow access to fc in dfu mode
  users = {
    groups.plugdev = {};
    users.dave = {
      extraGroups = [ "plugdev" ];
    };
  };

  services.udev = {
    extraRules = ''
      ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="0664", GROUP="plugdev"
    '';
  };

  environment.systemPackages = with pkgs; [
    chromium
    unstable.betaflight-configurator
    opentx
  ];
}
