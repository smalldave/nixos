.PHONY: chromebook copycommon

chromebook: partitionchromebook generateconfig copychromebook install

vm: parted createluks generateconfig copyvm install

pc: parted createluks generateconfig copypc install

t480: partitiont480 generateconfig copyt480 install

partitionchromebook:
	sgdisk -Z /dev/sda
	# EFI 
	sgdisk -n 0:0:+512M -t 0:ef00 /dev/sda
	# Linux filesystem
	sgdisk -n 0:0:0 -t 0:8300 /dev/sda

	$(call setup_partitions, '/dev/sda1', '/dev/sda2')

partitiont480:
	# 1TB drive
	sgdisk -Z /dev/nvme0n1
	# EFI
	sgdisk -n 0:0:+1G -t 0:ef00 /dev/nvme0n1
	# Linux Filesystem
	sgdisk -n 0:0:0 -t 0:8300 /dev/nvme0n1
        
	$(call setup_partitions, '/dev/nvme0n1p1', '/dev/nvme0n1p2')

define setup_partitions
	cryptsetup luksFormat $2
	dd if=/dev/urandom of=./keyfile0.bin bs=1024 count=4
	cryptsetup luksAddKey $2 keyfile0.bin
	cryptsetup luksOpen $2 nixos 
	
	pvcreate /dev/mapper/nixos
	vgcreate vg /dev/mapper/nixos
	lvcreate -L 4G -n swap vg
	lvcreate -l '100%FREE' -n root vg

	mkfs.fat -F 32 $1 
	mkswap -L swap /dev/vg/swap
	mkfs.ext4 -L root /dev/vg/root

	mount /dev/vg/root /mnt
	mkdir -p /mnt/boot/efi
	mount $1 /mnt/boot/efi
	swapon /dev/vg/swap

	find keyfile*.bin -print0 | sort -z | cpio -o -H newc -R +0:+0 --reproducible --null | gzip -9 > /mnt/boot/initrd.keys.gz
	chmod 000 /mnt/boot/initrd.keys.gz
endef

parted:
	parted /dev/sda -- mklabel msdos
	parted -a optimal /dev/sda -- mkpart primary 1MiB -1GiB
	parted -a optimal /dev/sda -- mkpart primary linux-swap -1GiB 100%

createluks:
	cryptsetup luksFormat /dev/sda1
	cryptsetup luksOpen /dev/sda1 ${NIXOS_NAME}
	mkfs.ext4 -L ${NIXOS_NAME} /dev/mapper/${NIXOS_NAME}
	# Swap not working
	mkswap -L swap /dev/sda2
	mount /dev/mapper/${NIXOS_NAME} /mnt

copychromebook: copylaptop
	cp etc/chromebook.nix /mnt/etc/nixos/configuration.nix
	$(call replace_device_uuid, /dev/sda2)

copypc: copycommon
	cp etc/pc.nix /mnt/etc/nixos/configuration.nix

copyvm: copycommon
	cp etc/vm.nix /mnt/etc/nixos/configuration.nix

define replace_device_uuid
	sed -i "s/{{luks_uuid}}/$$(blkid -o value -s UUID $1)/g" /mnt/etc/nixos/configuration.nix
endef

copyt480: copylaptop
	cp etc/t480.nix /mnt/etc/nixos/configuration.nix
	$(call replace_device_uuid, /dev/nvme0n1p2)
	
copylaptop: copycommon
	cp etc/laptop.nix /mnt/etc/nixos/laptop.nix

copycommon:
	nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
	nix-channel --update nixos-unstable
	cp etc/common.nix /mnt/etc/nixos/common.nix
	cp etc/rc.nix /mnt/etc/nixos/rc.nix


generateconfig:
	nixos-generate-config --root /mnt

install:
	nixos-install
	nixos-enter --root /mnt
	#This does not work maybe try nixos-enter -c 'passwd dave' --root /mnt?
	#passwd dave
	#Dotfiles?
